package com.example.tp2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    int CALL_PHONE_PERMISSION = 1;
    int CALL_LOGIN_PERMISSION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // On récupère le numéro entré dans le champs EditText
        EditText get_phone_number = (EditText)findViewById(R.id.phone_number);

        // On récupère l'element logo téléphone pour cliquer dessus pour valider
        // le numero qui sera entré dans le cha;ps EditText
        ImageView phone = (ImageView)findViewById(R.id.img_phone);
        phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Quand on click sur l'image on récupère la valeur donné dans EditText
                // Et on converti en String la valeur.
                String phone_number = get_phone_number.getText().toString();
                Log.d("-- phone_number: ", phone_number);

                // Runtime Permission
                if (ContextCompat.checkSelfPermission(
                        MainActivity.this,
                        Manifest.permission.CALL_PHONE
                ) == PackageManager.PERMISSION_GRANTED) {
                    Log.d("PERMISSION", "GRANTED");
                    // On lance la nouvelle activity pour appeler le numéro qu'on met au bon format
                    // Uri tel:xxxx pour l'executer dans le nouveau composants telephone interne.
                    Intent callActivity = new Intent(
                            Intent.ACTION_CALL, Uri.parse("tel:" + phone_number)
                    );

                    System.out.println("****************************************************");
                    System.out.println(Uri.parse("tel:" + phone_number));
                    System.out.println("****************************************************");

                    startActivity(callActivity);
                } else {
                    // Ask for the permission.
                    Log.d("PERMISSION", "DENIED");
                    requestPermissions(
                            new String[] {Manifest.permission.CALL_PHONE},
                            CALL_LOGIN_PERMISSION
                    );
                }
            }
        });

        // Ici on récupère pareil, l'image browser
        ImageView browser = (ImageView)findViewById(R.id.img_browser);
        // Et hop quand on click dessus, ça lance automatiquement le site internet
        // entré en dure dans le code dans une nouvelle activity interne Navigateur.
        browser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserActivity = new Intent(Intent.ACTION_VIEW);
                browserActivity.setData(Uri.parse("https://lasalledutemps.fr"));
                startActivity(browserActivity);
            }
        });

        // On récupère l'image login en vu de la deuxieme question
        ImageView login = (ImageView)findViewById(R.id.img_login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("HELLO", "LOGIN");
                // On switch de notre activité vers l'app My Login App
                if (ContextCompat.checkSelfPermission(
                        MainActivity.this,
                        "composant.permission.CALL_LOGIN"
                ) == PackageManager.PERMISSION_GRANTED) {
                    Intent loginActivity = new Intent();
                    loginActivity.setAction("myloginapp.ACTION");

                    // Pour tester avec l'envoi de donnée
                    /*
                    loginActivity.setAction(Intent.ACTION_SEND);
                    loginActivity.putExtra(Intent.EXTRA_TEXT, "coucou");
                    loginActivity.setType("text/plain");
                    */
                    startActivity(loginActivity);
                } else if (shouldShowRequestPermissionRationale("composant.permission.CALL_LOGIN")) {
                    Toast.makeText(MainActivity.this, "Sans autoriser l'application, vous n'avez pas accès au Login.", Toast.LENGTH_SHORT).show();
                } else {
                    // Ask for the permission.
                     requestPermissions(
                            new String[]{"composant.permission.CALL_LOGIN"},
                            CALL_PHONE_PERMISSION
                    );
                }
            }
        });
    }

    protected void onStart() {
        super.onStart();
        Log.i("Main Activity", "------------------------ ON START");
    }
    protected void onPause() {
        super.onPause();
        Log.i("Main Activity", "------------------------ ON PAUSE");
    }
    protected void onResume() {
        super.onResume();
        Log.i("Main Activity", "------------------------ ON RESUME");
    }
    protected void onStop() {
        super.onStop();
        Log.i("Main Activity", "------------------------ ON STOP");
    }
    protected void onDestroy() {
        super.onDestroy();
        Log.i("Main Activity", "------------------------ ON DESTROY");
    }
}