package com.example.myloginapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText txtUserName=(EditText)this.findViewById(R.id.txtUsername);
        EditText txtPassword=(EditText)this.findViewById(R.id.txtPwd);
        Button btnLogin=(Button)this.findViewById(R.id.btnLogin);
        Button btnCancel=(Button)this.findViewById(R.id.btnCancel);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtUserName.getText().toString().equals(txtPassword.getText().toString())){
                    Toast.makeText(
                            MainActivity.this, "Login autorisé", Toast.LENGTH_SHORT
                    ).show();
                } else {
                    Toast.makeText(
                            MainActivity.this, "Non autorisé", Toast.LENGTH_SHORT
                    ).show();
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}